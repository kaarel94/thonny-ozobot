Ozobot plug-in for Thonny
==========================

This is a plug-in for `Thonny IDE <http://thonny.org>`_, which adds possibility to program `Ozobot robot <http://ozobot.com/>`_ in a `Python-like language <https://github.com/Kaarel94/Ozobot-Python/>`_.

Usage
------

#. Install Thonny 2.1 or later
#. Select Tools => Manage plug-ins...
#. enter ``thonny-ozobot`` to search box and press ENTER
#. Install the plug-in
#. Restart Thonny

After this you should see ``Create ozobot color sequence`` command in Tools menu